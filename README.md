# IX Form Validation
#### jQuery плагин для валидации форм

Version: 1.0

Автор: <a href="https://t.me/inevix" target="_blank">inevix</a><br/>
Сайт: <a href="https://inevix.biz/" target="_blank">inevix.biz</a><br/>
E-mail: <a href="mailto:ineviix@gmail.com" target="_blank">ineviix@gmail.com</a><br/>

## Как подключить:
Подключить к проекту минифицированный js файл `ix-modal.js` из папки `public` после закрывабщегося тэга `</body>` и после подключённой библиотеки jquery.
<br/><br/>
Пример:<br/>
`<script type="text/javascript" src="public/ix-modal.js"></script>`

## Как вызвать функцию

1. Вызвать функцию со стандартный настройками:
<br/><br/>
`<script type="text/javascript">`<br/>
    `$(document).ready(function() {`<br/>
        `ixFormValidation();`<br/>
    `});`<br/>
`</script>`

2. Вызвать функцию Заменить стандартный класс форм:
<br/><br/>
`<script type="text/javascript">`<br/>
    `$(document).ready(function() {`<br/>
        `ixFormValidation({`<br/>
            `classForms: 'another-class'`<br/>
        `});`<br/>
    `});`<br/>
`</script>`

## Все доступные опции с дефолтными значениями

- Класс форм: <br/>`classForms: 'js-form'`<br/><br/>
- Класс Control Holder: <br/>`classHolders: 'js-control-holder'`<br/><br/>
- Класс Control Check: <br/>`classCheck: 'js-control-check'`<br/><br/>
- Класс Control Select: <br/>`classSelect: 'js-control-select'`<br/><br/>
- Показывать лейблы над полем ввода: <br/>`showLabels: true`<br/><br/>
- Класс лейблов: <br/>`classLabels: 'js-label'`<br/><br/>
- Класс инпутов: <br/>`classInputs: 'js-form-control'`<br/><br/>
- Класс кнопки submit: <br/>`classSubmit: 'js-submit'`<br/><br/>
- Показывать ошибки под полем ввода: <br/>`showErrors: true`<br/><br/>
- Если true, то при заполнении одного из полей e-mail или телефон, второе заполнять не обязательно: <br/>`requiredOne: true`<br/><br/>
- Ошибка, если поле пустое: <br/>`errorEmpty: 'This field is required!'`<br/><br/>
- Ошибка, если недостаточно символов: <br/>`errorLength: 'Please enter more 1 character!'`<br/><br/>
- Ошибка, если некорректный e-mail: <br/>`errorEmail: 'Incorrect e-mail address!'`<br/><br/>
- Ошибка, если некорректный номер телефона,: <br/>`errorPhone: 'Incorrect phone number!'`<br/><br/>
- Ошибка, если не выбрана радио-кнопка или чекбокс: <br/>`errorCheck: 'Please select one button!'`<br/><br/>
- Ошибка, если не выбран пункт выпадающего списка: <br/>`errorSelect: 'Please select one item!'`<br/><br/>
- Текст сообщения ошибки отправки: <br/>`errorSend: 'Message not sent!'`<br/><br/>
- Текст сообщения успешной отправки: <br/>`successSend: 'Message sent successfully!'`<br/><br/>
- Минимальное количество цифр в номере (без учёта символов и пробелов): <br/>`phoneMinLength: 5`<br/><br/>
- Путь к файлу, который будет принимать данные: <br/>`fileURL: '../feedback.php'`

Также доступно редактирование масок телефона и e-mail'а:
- Маска для e-mail'a: <br/>`emailMask`<br/><br/>
- Недопустимые символы в номере телефона <br/>`phoneMaskBefore`<br/><br/>
- Символы, которые не будут учитываться при подсчёте количества цифр в номере телефона <br/>`phoneMaskAfter`

## Пример использования html атрибутов

Можно посмотреть в файле `index.html`
- `<form class="js-form" data-validation="true">`
- `<input class="js-form-control" data-control="length" data-required="true"/>`
- `<input class="js-form-control" data-control="email" data-required="true"/>`
- `<input class="js-form-control" data-control="phone" data-required="true"/>`
- `<div class="js-control-check" data-control="check" data-required="true">`<br/>
   `<label class="check-label">`<br/>
       `<input class="js-form-control" type="radio|checkbox">`<br/>
       `<span>Radio 1</span>`<br/>
   `</label>`<br/>
`</div>`
- `<div class="js-control-select" data-control="select" data-required="true">`<br/>
   `<select class="js-form-control">`<br/>
       `<option disabled selected value="1">Choose select</option>`<br/>
       `<option value="">Value 1</option>`<br/>
   `</select>`<br/>
`</div>`

## Как редактировать:
Выполнить команду в терминале: `npm i --save-dev`

Запустить компиляцию командой: `gulp build`<br/>
или запустить наблюдателя, который будет компилировать постоянно при изменении скрипта: `gulp`

Редактировать файл: `source/ix-form-validation.js`