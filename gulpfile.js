'use strict';

let path = {
        build: {
            js: 'public/'
        },
        src: {
            js: 'source/ix-form-validation.js'
        },
        watch: {
            js: 'source/**/*.js'
        }
    },
    gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel');

function buildJs() {
    return gulp
    .src(path.src.js)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
        presets: ['@babel/env', 'minify'],
        comments: false
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(path.build.js));
}

function watchFiles() {
    gulp.watch(path.watch.js, buildJs);
}

exports.build = gulp.parallel(buildJs);
exports.default = gulp.parallel(watchFiles);